## Archive & Restore Infrastructure Downtime due to Maintenance.
8.12.2023 - 13.12.2023

Due to essential upgrades which will be carried out on the backend archive infrastructure, DMA Archive and Restore actions will be paused during this time. 

Any archive or restore requests that are made after 12 Noon on Friday December 8th, will be queued and processed once the maintenance on the backend infrastructure is complete.

We expect the archive and restore services to become available again by Wednesday 13 December 2023 at the latest. Once available we will begin to process all queued jobs. Please be patient as this may take several days to clear out the queue. 

As usual, you will be notified by email once your jobs have been processed. You can also track the status of your jobs via the DMA.

We appreciate your cooperation and patience.

If you have any questions please contact us at dma@embl.de





