# Restore

Data restoration involves retrieving data from the tape and placing it back into your group's shared storage.

When data has been archived, a list of accessible archives appears in the 'Archive & Restore' section within the Data overview panel. To restore an archive, simply click the "Restore" button next to the desired archive. You'll need to specify a destination path for the restored data. By default, this is set to the original parent directory of the data, but you can modify it as needed.

The data will continue to be stored on the tape archive even after restoration, allowing for future restorations if necessary.

Be aware that the restoration process is time-consuming. The duration depends on the data size and the current load on the archive infrastructure, potentially taking several days. You will be informed by email once your restore request has been completed.

Please ensure that the chosen restore location has **sufficient space** to accommodate the restored data. The required space is displayed in the DMA UI when initiating restoration. If adequate space is not available, the restoration will fail, potentially causing storage issues for your group.

## Partial Restore
You can choose to restore specific subdirectories or files from a larger archive. To do this, refer to the Archive Log File, which lists all files and folders within an archive. 

When opting for partial restoration, you must specify the `Limit To` path exactly as it is shown in the Archive Log File.