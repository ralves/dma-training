# Delete

The DMA allows you to delete your data from your group share. To delete data you need to make sure you have the required file system permissions on all the data. If there are files / directories which you do not have permission to delete, the delete request will fail. 

Deletion of large data items may take some time. You will be informed by email once your delete request has been finished successfully. 

Please make sure that you archive data before deletion if you may need the data in the future. We may not be able to restore data which is deleted without first being archived.