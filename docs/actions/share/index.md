# Share

## Group Volume Data
The DMA provides a way for you to share your group volume based data with other EMBL members by adjusting the file system permissions, thereby granting them access.

You can share your DMA registered data with any member of EMBL. Within the 'Share' section, simply input the EMBL username of the individual and define their access level (Read Only / Read & Write). There's also an option to include a comment, which will be conveyed to the user.

Upon successful data sharing, both you and the recipient will be notified via email about the completion of the sharing process.

Please be aware that **you need to be the owner** of the data you are sharing. If any part of the data is owned by someone else in the file system, the sharing process will not complete successfully, resulting in the data being partially shared.

### Permission Inheritance for Data Transferred into a DMA Shared Directory
When sharing a directory via the DMA with Read + Write access, users can add new data. To ensure proper permission inheritance, data should be either created directly within the shared directory or copied into it using the `cp` command by the users who the directory is shared with. 

This is crucial because copying, i.e. using the `cp` command, creates a new instance of the data, inheriting the directory's permissions. In contrast, moving the data with the `mv` command can lead to permission inconsistencies. This occurs because `mv` transfers both the data and its original permissions, bypassing the shared directory's permission which were applied via the DMA.

## Buckets
S3 Buckets which are created via the DMA can be shared with any EMBL user. Just like file system based sharing, you can specify the level of permissions which the user should have (Read Only / Read & Write). Once the bucket is shared, the user will be informed by email and will be able to access the data via the EMBL Minio UI. 

## Shared Data View
The `Shared Data` view on the left hand menu allows you to quickly see all data which other users have shared with you via the DMA.